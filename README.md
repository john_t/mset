# MSet

Mset allows you to tag music files, easily.

## Example

```bash
# Set the author to "Franklin D. Roosevelt",
# the album to "The New Album"
# the track title to "Shoot The Pigs"
# the genre to the "Revival"
# It is track 2 of 13.
mset -a "Franklin D. Roosevelt" \
     -A "The New Album" \
     -n "Shoot the pigs" \
     -g "Revival" \
     -t 2 \
     -T 13 \
     02_shoot_the_pigs.flac
```
