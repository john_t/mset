use std::{
    fs::File,
    path::{Path, PathBuf},
    str::FromStr,
};

use serde_json::json;
use yansi::Paint;

mod image;
use anyhow::{Context, Result};
use clap::Parser;
use lofty::{Accessor, AudioFile, Picture, Tag, TaggedFileExt};

use crate::image::PcTypeWrapper;

macro_rules! set_property {
    ($args:ident, $tag:ident, $name:ident, $set:ident, $remove:ident) => {
        if let Some(ref value) = $args.$name {
            match value {
                Some(value) => {
                    eprintln!(
                        "{} {}",
                        "Setting".bright_green().bold(),
                        stringify!($name)
                    );
                    $tag.$set(value.clone());
                }
                None => {
                    eprintln!(
                        "{} {}",
                        "Removing".red().bold(),
                        stringify!($name)
                    );
                    $tag.$remove();
                }
            }
        }
    };
}

/// Tag your music, but easily.
#[derive(Parser, Debug)]
pub struct Args {
    /// View the properties at the end
    #[clap(long, short)]
    pub view: bool,
    /// Set the artist property
    #[clap(long, short)]
    pub artist: Option<Option<String>>,
    /// Set the title property
    #[clap(long, short = 'n')]
    pub title: Option<Option<String>>,
    /// Set the album property
    #[clap(long, short = 'A')]
    pub album: Option<Option<String>>,
    /// Set the genre property
    #[clap(long, short)]
    pub genre: Option<Option<String>>,
    /// Set the track property
    #[clap(long, short)]
    pub track: Option<Option<u32>>,
    /// Set the track total property
    #[clap(long, short = 'T')]
    pub track_total: Option<Option<u32>>,
    /// Set the disk property
    #[clap(long, short)]
    pub disk: Option<Option<u32>>,
    /// Set the disk total property
    #[clap(long, short = 'D')]
    pub disk_total: Option<Option<u32>>,
    /// Set the year property
    #[clap(long, short)]
    pub year: Option<Option<u32>>,
    /// Set the comment property
    #[clap(long, short)]
    pub comment: Option<Option<String>>,
    /// Provide an image for the album
    ///
    /// This should be in the form of `{type}={path to image}`.  Leave
    /// `{path to image}` blank to remove this type of image.
    ///
    /// Type can be one of:
    /// other, icon, other_icon, cover_front (aliases: front, f), cover_back,
    /// leaflet, media, lead_artist, artist, conductor, band, composer,
    /// lyricist, recording_location, during_recording, during_performance,
    /// screen_capture, bright_fish, illustration, band_logo, publisher_logo,
    #[clap(long = "image", short)]
    pub images: Vec<image::ImageSet>,
    /// Files
    pub files: Vec<PathBuf>,
}

fn main() -> Result<()> {
    let args = Args::parse();
    for path in &args.files {
        let dp = path.display();
        eprintln!("{}:", dp.bold());
        let mut tagged_file = lofty::read_from_path(&path)?;

        if !tagged_file.contains_tag() {
            let tag_type = tagged_file.primary_tag_type();
            eprintln!(
                "{} new primary tag ({:?})",
                "Inserting".cyan().bold(),
                tag_type
            );
            tagged_file.insert_tag(Tag::new(tag_type));
        }

        let tag = tagged_file.primary_tag_mut();
        let tag = match tag {
            Some(tag) => tag,
            None => tagged_file.first_tag_mut().unwrap(),
        };

        handle_properties(&args, tag);
        handle_images(&args, tag)?;

        if args.view {
            display(path, tag);
        }

        tagged_file.save_to_path(&path)?;
    }

    Ok(())
}

fn handle_properties(args: &Args, tag: &mut Tag) {
    set_property!(args, tag, artist, set_artist, remove_artist);
    set_property!(args, tag, title, set_title, remove_title);
    set_property!(args, tag, album, set_album, remove_album);
    set_property!(args, tag, genre, set_genre, remove_genre);
    set_property!(args, tag, track, set_track, remove_track);
    set_property!(args, tag, track_total, set_track_total, remove_track_total);
    set_property!(args, tag, disk, set_disk, remove_disk);
    set_property!(args, tag, disk_total, set_disk_total, remove_disk_total);
    set_property!(args, tag, year, set_year, remove_year);
    set_property!(args, tag, comment, set_comment, remove_comment);
}

fn handle_images(args: &Args, tag: &mut Tag) -> Result<(), anyhow::Error> {
    Ok(for image in &args.images {
        match &image.path {
            Some(path) => {
                let mut file = File::open(&path)?;
                let mut picture = Picture::from_reader(&mut file)?;
                picture.set_pic_type(image.typ);
                tag.push_picture(picture);
                eprintln!(
                    "{} image of type {}",
                    "Setting".bright_green().bold(),
                    PcTypeWrapper(image.typ)
                );
            }
            None => {
                let picture_count = tag
                    .pictures()
                    .into_iter()
                    .filter(|x| x.pic_type() == image.typ)
                    .count();

                // Print information
                eprintln!(
                    "{} {picture_count} {} of type {}",
                    "Removing".red().bold(),
                    if picture_count == 1 {
                        "image"
                    } else {
                        "images"
                    },
                    PcTypeWrapper(image.typ)
                );

                tag.remove_picture_type(image.typ);
            }
        }
    })
}

fn display(path: &Path, tag: &mut Tag) {
    let value = json!({
        "file": path.display().to_string(),
        "value": {
            "artist": tag.artist(),
            "title": tag.title(),
            "album": tag.album(),
            "genre": tag.genre(),
            "track": tag.track(),
            "track_total": tag.track_total(),
            "disk": tag.disk(),
            "disk_total": tag.disk_total(),
            "year": tag.year(),
            "comment": tag.comment(),
            "pictures": tag
                .pictures()
                .iter()
                .map(|x| json!({
                    "type": PcTypeWrapper(x.pic_type()).to_string(),
                    "id3v2": x.pic_type().as_u8(),
                    "ape_key": x.pic_type().as_ape_key(),
                })).collect::<Vec<_>>(),
        }
    });
    println!("{}", value.to_string())
}
