use std;
use std::fmt::Display;
use std::fmt::Formatter;
use std::path::PathBuf;
use std::str::FromStr;

use anyhow;
use anyhow::bail;
use anyhow::Context;
use anyhow::Result;
use lofty::PictureType;
use yansi::Paint as _;

#[derive(Debug, Clone, Copy)]
pub struct PcTypeWrapper(pub PictureType);

impl FromStr for PcTypeWrapper {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<PcTypeWrapper> {
        Ok(PcTypeWrapper(match s {
            "other" => PictureType::Other,
            "icon" => PictureType::Icon,
            "other_icon" => PictureType::OtherIcon,
            "cover_front" | "front" | "f" => PictureType::CoverFront,
            "cover_back" => PictureType::CoverBack,
            "leaflet" => PictureType::Leaflet,
            "media" => PictureType::Media,
            "lead_artist" => PictureType::LeadArtist,
            "artist" => PictureType::Artist,
            "conductor" => PictureType::Conductor,
            "band" => PictureType::Band,
            "composer" => PictureType::Composer,
            "lyricist" => PictureType::Lyricist,
            "recording_location" => PictureType::RecordingLocation,
            "during_recording" => PictureType::DuringRecording,
            "during_performance" => PictureType::DuringPerformance,
            "screen_capture" => PictureType::ScreenCapture,
            "bright_fish" => PictureType::BrightFish,
            "illustration" => PictureType::Illustration,
            "band_logo" => PictureType::BandLogo,
            "publisher_logo" => PictureType::PublisherLogo,
            _ => bail!("Unknown picture type"),
        }))
    }
}

impl Display for PcTypeWrapper {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self.0 {
            PictureType::Other => write!(f, "other"),
            PictureType::Icon => write!(f, "icon"),
            PictureType::OtherIcon => write!(f, "other_icon"),
            PictureType::CoverFront => write!(f, "cover_front"),
            PictureType::CoverBack => write!(f, "cover_back"),
            PictureType::Leaflet => write!(f, "leaflet"),
            PictureType::Media => write!(f, "media"),
            PictureType::LeadArtist => write!(f, "lead_artist"),
            PictureType::Artist => write!(f, "artist"),
            PictureType::Conductor => write!(f, "conductor"),
            PictureType::Band => write!(f, "band"),
            PictureType::Composer => write!(f, "composer"),
            PictureType::Lyricist => write!(f, "lyricist"),
            PictureType::RecordingLocation => write!(f, "recording_location"),
            PictureType::DuringRecording => write!(f, "during_recording"),
            PictureType::DuringPerformance => write!(f, "during_performance"),
            PictureType::ScreenCapture => write!(f, "screen_capture"),
            PictureType::BrightFish => write!(f, "bright_fish"),
            PictureType::Illustration => write!(f, "illustration"),
            PictureType::BandLogo => write!(f, "band_logo"),
            PictureType::PublisherLogo => write!(f, "publisher_logo"),
            x => write!(f, "[other: {}]", x.as_u8()),
        }
    }
}

#[derive(Clone, Debug)]
pub struct ImageSet {
    pub typ: PictureType,
    pub path: Option<PathBuf>,
}

impl FromStr for ImageSet {
    type Err = anyhow::Error;
    fn from_str(s: &str) -> Result<Self> {
        // Split the assignment over the equals sign
        let (s0, s1) = s.split_once('=').context(
            "Images should be in the form of `{type}={path to image}`",
        )?;

        if s1.starts_with('~') {
            eprintln!(
                "{}: Image assignment starts with `~`;",
                "Warning".bright_yellow().bold()
            );
            eprintln!(
                "{}: Most shells do not expand `~` in the \
                middle of strings.  Use $HOME instead.",
                "   Note".bold()
            );
        }

        Ok(ImageSet {
            typ: PcTypeWrapper::from_str(s0)?.0,
            path: if s1.is_empty() {
                None
            } else {
                Some(s1.parse()?)
            },
        })
    }
}
